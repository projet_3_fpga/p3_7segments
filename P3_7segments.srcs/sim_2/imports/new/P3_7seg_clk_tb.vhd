----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06/09/2021 06:23:32 PM
-- Design Name: 
-- Module Name: P3_7seg_clk_tb - stimulus
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity P3_7seg_clk_tb is
end P3_7seg_clk_tb;

architecture stimulus of P3_7seg_clk_tb is
  constant clk_period : time := 1ns; -- 1GHz
  signal clk_in : std_logic;
  signal en : std_logic;
  signal clk_7seg : std_logic;
begin

  DUT : entity work.P3_7seg_clk(rtl)
    port map (
      clk_in => clk_in,
      en => en,
      clk_7seg => clk_7seg
    );

  en <= '0', '1' after 2*clk_period;
  
  drive_clk: process begin
    clk_in <= '0';
    wait for clk_period / 2;
    clk_in <= '1';
    wait for clk_period / 2;    
  end process drive_clk;
  
end stimulus;
