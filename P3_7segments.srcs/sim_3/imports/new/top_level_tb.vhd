----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06/09/2021 07:14:07 PM
-- Design Name: 
-- Module Name: top_level_tb - stimulus
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity top_level_tb is
end top_level_tb;

architecture stimulus of top_level_tb is
  constant clk_period : time := 1ns; -- 100MHz
  signal enable : std_logic_vector(3 downto 0);
  signal seven_segment : std_logic_vector(7 downto 0);
  signal sw_in : std_logic_vector(3 downto 0);
  signal CLK1 : std_logic;
begin

  DUT : entity work.top_level(rtl)
    port map (
      enable => enable,
      seven_segment => seven_segment,
      sw_in => sw_in,
      CLK1 => CLK1
    );
  
  sw_in <= "1111", "1110" after 100ns;

  drive_clk: process begin
  CLK1 <= '0';
  wait for clk_period / 2;
  CLK1 <= '1';
  wait for clk_period / 2;    
  end process drive_clk;
  
end stimulus;
