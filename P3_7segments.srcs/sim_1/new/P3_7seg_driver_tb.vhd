----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06/08/2021 06:58:55 PM
-- Design Name: 
-- Module Name: P3_7seg_driver_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity P3_7seg_driver_tb is
end P3_7seg_driver_tb;

architecture test of P3_7seg_driver_tb is
  constant clk_period : time := 1 ns; -- 1 GHz

  signal print_data : std_logic_vector (31 downto 0);
  alias char_1 is print_data(31 downto 24);
  alias char_2 is print_data(23 downto 16);
  alias char_3 is print_data(15 downto 8);
  alias char_4 is print_data(7 downto 0);

  signal segments : std_logic_vector(7 downto 0);
  signal ds_en : std_logic_vector(3 downto 0);
  signal en : std_logic;
  signal clk : std_logic;
begin

  DUT : entity work.P3_7seg_driver(rtl)
    port map (
      print_data => print_data,
      segments => segments,
      ds_en => ds_en,
      en => en,
      clk => clk
    );
  
  drive_clk: process begin
    clk <= '0';
    wait for clk_period / 2;
    clk <= '1';
    wait for clk_period / 2;    
  end process drive_clk;
  
  stimulus: process begin
    en <= '0';
    wait for 2*clk_period;  
    char_1 <= "01101110"; -- H
    char_2 <= "10011110"; -- E
    char_3 <= "00011100"; -- L
    char_4 <= "11111101"; -- O. (note the decimal point)
    en <= '1';    

    -- Should see segments cycle through char_1 to char_4
    wait for 6*clk_period;
    
    char_2 <= "11111100"; -- O
    char_4 <= "11101111"; -- A. (note the decimal point)

    wait for 6*clk_period; -- test complete
    en <= '0';
    wait;
  end process stimulus;
  
end test;
