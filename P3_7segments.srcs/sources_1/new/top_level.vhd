----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06/08/2021 04:51:22 PM
-- Design Name: 
-- Module Name: top_level - rtl
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity top_level is
  port (
    enable        : out std_logic_vector(3 downto 0);
    seven_segment : out std_logic_vector(7 downto 0);
    sw_in         : in std_logic_vector(3 downto 0);
    CLK1          : in std_logic
  );
end top_level;
    
architecture rtl of top_level is
  signal print_data : std_logic_vector(31 downto 0);
  alias char_1 is print_data(31 downto 24);
  alias char_2 is print_data(23 downto 16);
  alias char_3 is print_data(15 downto 8);
  alias char_4 is print_data(7 downto 0);
  
  signal en : std_logic;
  signal pre_clk : std_logic;
  signal clk_buffed : std_logic;
  signal clk_7seg : std_logic;
  
  signal seven_segment_n : std_logic_vector(7 downto 0);
  signal enable_n : std_logic_vector(3 downto 0);
  signal sw_in_n : std_logic_vector(3 downto 0);

  component clk_wiz_0
  port
   (-- Clock in ports
    -- Clock out ports
    pre_clk : out    std_logic;
    CLK1    : in     std_logic
   );
  end component;
  
begin

  
  MA7_7seg: entity work.P3_7seg_driver(rtl)
  port map (
    print_data => print_data,
    segments(7) => seven_segment_n(2), -- A
    segments(6) => seven_segment_n(3), -- B
    segments(5) => seven_segment_n(5), -- C
    segments(4) => seven_segment_n(6), -- D
    segments(3) => seven_segment_n(7), -- E
    segments(2) => seven_segment_n(1), -- F
    segments(1) => seven_segment_n(0), -- G
    segments(0) => seven_segment_n(4), -- DP
    ds_en => enable_n,
    en => en,
    clk => clk_7seg
  );
  
  BUFG_inst : BUFG
  port map (
     O => clk_buffed,
     I => pre_clk
  );
  
  MMCM_clk_div20 : clk_wiz_0
     port map ( 
    -- Clock out ports  
     pre_clk => pre_clk,
     -- Clock in ports
     CLK1 => CLK1
   );

  
  P3_7seg_clk_inst: entity work.P3_7seg_clk(rtl)
  port map (
    clk_in => clk_buffed,
    en => en,
    clk_7seg => clk_7seg
  );
  
  char_1 <= "01101110"; -- H
  char_2 <= "10011110"; -- E. (note the decimal point)
  char_3 <= "00011100"; -- L
  char_4 <= "11111101"; -- O. (note the decimal point)

  en <= sw_in_n(0);
  
  -- Accomodates the inverted logic of 7-segments and their enable lines
  seven_segment <= not seven_segment_n;
  enable <= not enable_n;
  sw_in_n <= not sw_in;

end rtl;
