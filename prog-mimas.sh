#!/bin/bash

# $1: Fichier bin à flasher
openocd -f numato_mimasa7.cfg  -c "init" -c "pld load 0 $1" -c "shutdown"
exit 0

